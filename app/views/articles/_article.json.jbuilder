json.extract! article, :id, :name, :text, :article_type, :story_id, :created_at, :updated_at
json.url article_url(article, format: :json)
