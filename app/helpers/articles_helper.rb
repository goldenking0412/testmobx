module ArticlesHelper
  def get_article_type_from_id(id)
    return ArticleType.where(:id => id).limit(1)[0].type_name
  end

  def get_story_name_from_id(id)
    return Story.where(:id => id).limit(1)[0].name
  end
end
