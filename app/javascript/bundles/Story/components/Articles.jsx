import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Article from '../components/Article';
import articleStore from '../store/articleStore';

// @inject('articleStore')
@observer
class Articles extends Component {

  componentDidMount() {
    $("#Articles").DataTable({
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'os',
        selector: 'td:first-child'
      },
      order: [[ 1, 'asc' ]]
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      sorted : false,
      sorted_up : false
    }
  }

  /**
   * @param props - Comes from your rails view.
   */

  render() {
    // const { articleStore } = this.props.articleStore;
    const articles_data = articleStore.articles;
    let articles = [];
    let i = 0;
    while(articles_data[i] != undefined) {
      articles.push(
        <Article name={articles_data[i].name} text={articles_data[i].text} type={articles_data[i].article_type.toString()} article_id={articles_data[i].id} key={i}></Article>
      );
      i++;
    }
    return (
      <table id="Articles">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Text</th>
            <th>Type</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {articles}
        </tbody>
      </table>
    );
  }
}
export default Articles;