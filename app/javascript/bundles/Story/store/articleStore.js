import { observable, action } from 'mobx';
import ArticleApi from '../services/ArticleApi';

class ArticleStore {

  @observable articles = [];

  constructor(articles = []) {
    this.articles = articles;
    this.articleApi = new ArticleApi();
    this.articleApi.getArticles().
      then(articles => {
        this.setArticles(articles);
      });
  }

  @action addArticle = (article) => {
    this.articles.push(article);
  }

  @action setArticles = (articles) => {
    this.articles = [];
    let i = 0;
    while(articles[i] != undefined) {
      this.articles.push(articles[i]);
      i++;
    }

    // this.articleApi.subscribeArticle(() => {
    //   debugger
    //   this.articleApi.getArticles().
    //     then(articles => {
    //       this.setArticles(articles);
    //     });
    // })
  }
}

const store = new ArticleStore();
export default store;