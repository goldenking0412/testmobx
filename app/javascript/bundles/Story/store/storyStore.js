import { observable } from 'mobx';

class StoryStore {

  @observable stories;

  constructor(stories = []) {
    this.stories = stories;
  }

}

const storyStore = new StoryStore();
export default storyStore;
export { StoryStore };