class ArticlesController < ApplicationController
  helper ArticlesHelper
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_extra_data, only: [:show, :edit, :new]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article_types = ArticleType.all
  end

  # GET /articles/new
  def new
    @article = Article.new
    @article_types = ArticleType.all
  end

  # GET /articles/1/edit
  def edit
    @article_types = ArticleType.all
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        ActionCable.server.broadcast("notification_channel", {
          data: 'update'
        })
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        ActionCable.server.broadcast("notification_channel", {
          data: 'update'
        })
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      ActionCable.server.broadcast("notification_channel", {
        data: 'update'
      })
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def query
    if params[:search]
      if params[:column] == "name"
        render json: Article.search_by_name(params[:data]).to_json
      elsif params[:column] == "text"
        render json: Article.search_by_text(params[:data]).to_json
      else
        render json: []
      end
    elsif params[:sort]
      if params[:column] == "name"
        render json: Article.sort_by_name.to_json
      elsif params[:column] == "text"
        render json: Article.sort_by_text.to_json
      elsif params[:column] == "type"
        render json: Article.sort_by_type.to_json
      end
    elsif params[:group]
      if params[:column] == "name"
        render json: Article.group_by_name.to_json
      elsif params[:column] == "text"
        render json: Article.group_by_text.to_json
      elsif params[:column] == "type"
        render json: Article.group_by_type.to_json
      end
    elsif params[:story_group]
      if params[:type] == "article_count"
        render json: Article.group_story_article_count.to_json
      elsif params[:type] == "article_type_count"
        render json: Article.group_story_type_count.to_json
      elsif params[:type] == "sort_by_last_created_article"
        render json: Article.group_story_sort_last_created.to_json
      end
    end
  end

  def all
    render json: Article.all_articles.to_json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    def set_extra_data
      @article_types = ArticleType.all
      @stories = Story.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:name, :text, :article_type, :story_id)
    end
end
