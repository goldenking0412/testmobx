import PropTypes from 'prop-types';
import React from 'react';
import { observer } from 'mobx-react';

@observer
export default class Article extends React.Component {
  static propTypes = {
    aid : PropTypes.number,
    name: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.string
  };

  /**
   * @param props - Comes from your rails view.
   */

  constructor(props) {
    super(props);
    this.state = {
      aid : props.article_id,
      name: props.name,
      text: props.text,
      type: props.type
    }
  }

  render() {
    return (
      <tr>
        <td>
          <input type="checkbox" />
        </td>
        <td>{this.state.name}</td>
        <td>{this.state.text}</td>
        <td>{this.state.type}</td>
        <td>
          <a href={"/articles/"+this.state.aid}>
            <button value="">view</button>
          </a>
        </td>
        <td>
          <a href={"/articles/"+this.state.aid+"/edit"}>
            <button value="">edit</button>
          </a>
        </td>
        <td>
          <a href={"/articles/"+this.state.aid} data-method="delete">
            <button value="">destroy</button>
          </a>
        </td>
      </tr>
    );
  }
}
