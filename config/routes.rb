Rails.application.routes.draw do
  mount ActionCable.server, at: '/cable'
  
  root :to => 'application#index'

  get '/articles/query', to: 'articles#query'
  get '/articles/all', to: 'articles#all'
  resources :articles
  resources :article_types
  resources :stories
  
  get 'hello_world', to: 'hello_world#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
