import React from 'react';
import { Provider } from 'mobx-react';
import articleStore from '../store/articleStore';
import Articles from '../components/Articles';

export default (props, _railsContext) => {
  const store = {
    articleStore
  }
  store.articleStore.setArticles(props.articles);
  store.articleStore.articleApi.subscribeArticle(() => {
      store.articleStore.articleApi.getArticles().
        then((articles) => {
          store.articleStore.setArticles(articles);
          window.location.reload();
        });
    })

  return (
    <Provider {...store}>
      <Articles />
    </Provider>
  );
}
