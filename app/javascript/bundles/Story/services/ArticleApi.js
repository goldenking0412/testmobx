import ActionCable from 'actioncable';

export default class ArticleApi {
  constructor() {
    this.cable = ActionCable.createConsumer('/cable');
    this.subscription = false;
  }

  createArticle = (name) => {
    return fetch('/articles', {
      method: 'post',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        article: {name}
      })
    }).
    then(response => response.json());
  }

  subscribeArticle = (callback) => {
    this.subscription = this.cable.subscriptions.create(
      { channel: "NotificationChannel" },
      {
        received: callback
      }
    );
  }

  getArticles = () => {
    return fetch('/articles/all', {
      method: 'get',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
    }).
    then(response => response.json());
  }

  // postCheckin = (owner_uuid, lat, lon, captured_at) => {
  //   this.subscription.send({
  //     owner_uuid,
  //     lat,
  //     lon,
  //     captured_at
  //   });
  // }
}
