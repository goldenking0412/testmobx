import ReactOnRails from 'react-on-rails';

// import Article from '../bundles/Story/components/Article';
import ViewArticlesContainer from '../bundles/Story/containers/ViewArticlesContainer';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
  ViewArticlesContainer
});
