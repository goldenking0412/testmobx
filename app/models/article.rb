class Article < ApplicationRecord
  belongs_to :story
  def self.search_by_name(query_str)
    query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE name like '%#{query_str}%'"
    articles = ActiveRecord::Base.connection.exec_query(query)
    return articles
  end

  def self.search_by_text(query_str)
    query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE text like '%#{query_str}%'"
    articles = ActiveRecord::Base.connection.exec_query(query)
    return articles
  end

  def self.sort_by_name
    query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type ORDER BY name"
    articles = ActiveRecord::Base.connection.exec_query(query)
    # articles = Article.order(:name)
    return articles
  end

  def self.sort_by_text
    query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type ORDER BY text"
    articles = ActiveRecord::Base.connection.exec_query(query)
    # articles = Article.order(:text)
    return articles
  end

  def self.sort_by_type
    query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type ORDER BY type"
    articles = ActiveRecord::Base.connection.exec_query(query)
  end

  def self.group_by_name
    query = "SELECT DISTINCT name FROM articles"
    names = ActiveRecord::Base.connection.exec_query(query)
    results = {}
    names.rows.each { |name|
      query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE name like '%#{name[0]}%'"
      results[name[0]] = ActiveRecord::Base.connection.exec_query(query)
    }
    return results
  end

  def self.group_by_text
    query = "SELECT DISTINCT text FROM articles"
    texts = ActiveRecord::Base.connection.exec_query(query)
    results = {}
    texts.rows.each { |text|
      query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE text like '%#{text[0]}%'"
      results[text[0]] = ActiveRecord::Base.connection.exec_query(query)
    }
    return results
  end

  def self.group_by_type
    query = "SELECT DISTINCT article_type FROM articles"
    types = ActiveRecord::Base.connection.exec_query(query)
    results = {}
    types.rows.each { |type|
      query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE article_type = #{type[0]}"
      type_name = ArticleType.select("type_name").where("id = ?", type[0]).limit(1)[0].type_name
      results[type_name] = ActiveRecord::Base.connection.exec_query(query)
    }
    return results
  end

  def self.group_story_article_count
    results = {}
    query = "SELECT DISTINCT COUNT(id) FROM articles GROUP BY story_id"
    counts = ActiveRecord::Base.connection.exec_query(query)
    counts.rows.each { |count|
      results[count[0]] = []
      query = "SELECT story_id FROM (SELECT COUNT(id) as count, story_id FROM articles GROUP BY story_id) as count WHERE count = #{count[0]}"
      story_ids = ActiveRecord::Base.connection.exec_query(query)
      counter = 0
      story_ids.rows.each { |story_id|
        story_data = {}
        story_data["name"] = Story.where("id = #{story_id[0]}").limit(1)[0].name

        query = "SELECT articles.name, articles.text, article_types.type_name as type FROM articles INNER JOIN article_types ON article_types.id=articles.article_type WHERE story_id = #{story_id[0]}"
        articles = ActiveRecord::Base.connection.exec_query(query)
        story_data["articles"] = articles.rows
        results[count[0]].push(story_data)
        counter += 1
      }
    }
    return results
  end

  def self.group_story_type_count
    results = []
    query = "SELECT DISTINCT story_id FROM articles"
    story_ids = ActiveRecord::Base.connection.exec_query(query).rows

    query = "SELECT DISTINCT article_type FROM articles"
    article_types = ActiveRecord::Base.connection.exec_query(query).rows


    # "SELECT DISTINCT COUNT(CASE WHEN article_type = 1 then 1 END) as type1, COUNT(CASE WHEN article_type = 2 then 1 END) as type2, COUNT(CASE WHEN article_type = 3 then 1 END) as type3 FROM articles GROUP BY story_id"
    query = "SELECT DISTINCT"
    all_story_query = "SELECT story_id,"
    article_types.each { |type_id|
      query += " COUNT(CASE WHEN article_type = #{type_id[0]} then 1 END) as type#{type_id[0]},"
      all_story_query += " COUNT(CASE WHEN article_type = #{type_id[0]} then 1 END) as type#{type_id[0]},"
    }
    query = query[0...-1]
    all_story_query = all_story_query[0...-1]
    query += " FROM articles GROUP BY story_id"
    all_story_query += " FROM articles GROUP BY story_id"
    story_types = ActiveRecord::Base.connection.exec_query(query).rows  # [0,0,1], [1,0,0], [2,0,0]

    counter = 0
    story_types.each { |story_type|
      query = "SELECT story_id FROM (#{all_story_query}) AS t1 WHERE"
      count = 0
      article_types.each {|type_id|
        query += " type#{type_id[0]} = #{story_type[count]} AND"
        count += 1
      }
      query = query[0...-4]
      # query will be
      # SELECT story_id FROM (SELECT story_id, COUNT(CASE WHEN article_type = 1 then 1 END) as type1, COUNT(CASE WHEN article_type = 2 then 1 END) as type2, COUNT(CASE WHEN article_type = 3 then 1 END) as type3 FROM articles GROUP BY story_id) AS t1 WHERE type1=1 AND type2=0 AND type3=0
      sub_story_ids = ActiveRecord::Base.connection.exec_query(query).rows  # [1, 4]

      tres = {}
      count = 0
      story_type.each { |val|
        if val != 0
          query = "SELECT type_name FROM article_types WHERE id = #{article_types[count][0]}"
          art_type_name = ActiveRecord::Base.connection.exec_query(query).rows[0]
          tres[art_type_name[0]] = val
        end
        count += 1
      }
      tres["stories"] = []
      sub_story_ids.each {|story_id|
        tstory = Article.get_story_from_id(story_id[0])
        tres["stories"].push(tstory)
      }

      results.push(tres)
    }
    return results
  end

  def self.get_story_from_id(story_id)
    result = {}
    tstory = Story.where("id = ?", story_id).limit(1)[0]
    if tstory.nil?
      return []
    end
    result["story_name"] = tstory.name

    articles = Article.where("story_id = ?", story_id)
    result["articles"] = []
    articles.each { |article|
      ta = {}
      ta["name"] = article.name
      ta["text"] = article.text
      ta["type"] = Article.get_article_type_name_from_id(article.article_type)
      result["articles"].push(ta)
    }
    return result
  end

  def self.get_article_type_name_from_id(id)
    ttype = ArticleType.where("id = ?", id).limit(1)[0]
    if ttype.nil?
      return nil
    end
    return ttype.type_name
  end
  
  def self.group_story_sort_last_created
    # (SELECT * articles WHERE story_id = id)
    res = []
    query = "SELECT story_id from articles ORDER BY created_at DESC"
    all_story_ids = ActiveRecord::Base.connection.exec_query(query).rows
    ordered_ids = []
    all_story_ids.each { |story_id|
      if !ordered_ids.include?(story_id[0])
        ordered_ids.push(story_id[0])
      end
    }
    ordered_ids.each {|id|
      res.push(Article.get_story_from_id(id))
    }
    return res
  end

  def self.all_articles
    res = []
    query = "SELECT articles.name, articles.text, article_types.type_name as type, articles.id FROM articles INNER JOIN article_types ON article_types.id=articles.article_type"
    articles = ActiveRecord::Base.connection.exec_query(query).rows
    articles.each { |article|
      tart = {
        "name": article[0],
        "text": article[1],
        "article_type": article[2],
        "id": article[3]
      }
      res.push(tart)
    }
    return res
  end
end
